<?php

namespace Damd\TareasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Gopenux\GameBundle\Entity\Player;


use Damd\TareasBundle\Form\Type\TareaType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        //$em = $this->getDoctrine()->getManager();
        $tareas = $this->getDoctrine()
        ->getRepository('DamdTareasBundle:Tarea')
        ->findAll();
        return $this->render(
             'DamdTareasBundle:Default:index.html.twig', 
             array('tareas'=>$tareas));
    }
  
     
    public function newAction(Request $request)
    {
         $tarea = new Tarea();
         //$tarea->setTarea('Realizar merge para version 2');
         //$tarea->setFecha(new \DateTime('09/01/2015'));
         
         $form = $this->createFormBuilder($tarea)
              ->add('tarea','text')
              ->add('fecha','date')
              ->add('guardar','submit')
              ->getForm();
         
          $response = $this->forward('DamdTareasBundle:Default:procesarRequesTarea',
                                    array('form'=>$form,
                                          'tarea'=>$tarea));
         return $response;
    }
     
    public function newTypeAction(Request $request)
    {
          $tarea = new Tarea(); 
          $form = $this->createForm(new TareaType(), $tarea );
          $response = $this->forward('DamdTareasBundle:Default:procesarRequesTarea',
                                    array('form'=>$form,
                                          'tarea'=>$tarea));
         return $response;
         
    }
     
    public function newTypeServiceAction(Request $request)
    {
          $tarea = new Tarea(); 
          $form = $this->createForm('tarea', $tarea );
          $response = $this->forward('DamdTareasBundle:Default:procesarRequesTarea',
                                    array('form'=>$form,
                                          'tarea'=>$tarea));
         return $response;
    }
     
    public function procesarRequesTareaAction(Request $request,$form,$tarea)
    {
          //procesa el request
          $form->handleRequest($request);
         
          if($form->isValid())
          {
               $em = $this->getDoctrine()->getManager();
               $em->persist($tarea);
               $em->flush();
               return $this->redirect($this->generateUrl('damd_tareas_homepage'));
          }
          
         $arg = array('form'=>$form->createView());
         return $this->render('DamdTareasBundle:Default:new.html.twig',$arg); 
    }
     
  //src/Damd/JuegoBundle/Controller/DefaultController   
  //...   
    public function DocumentacionAPIAppAction(){
         
         
         
         $response = $this->render('DamdJuegoBundle:Default:doc.html.twig',$arg); 
         
         $response->setSharedMaxAge(200);
         
         return $response;
         
    }
     
     
   public function winnerplayer(){
        
         $player = $this->getDoctrine() 
               ->getRepository('GopenuxGamebundle:Player')
               ->getFirst(); 
        
        
   }  
    
}

<?php
//src/Damd/TareasBundle/Form/Type/TareaType.php
namespace Damd\TareasBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TareaType extends AbstractType
{
     public function buildForm(FormBuilderInterface $builder, array $options)
     {
          $builder
               ->add('tarea')
               ->add('fecha',null,array('widget'=>'single_text'))
               ->add('save','submit');
     }
     
     public function getName()
     {
         return 'tarea'; 
     }
}

?>
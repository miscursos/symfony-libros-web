<?php

namespace Damd\StoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Damd\StoreBundle\Entity\Producto;
use Symfony\Component\HttpFoundation\Response;
/*Use de ParamConverter*/
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('DamdStoreBundle:Default:index.html.twig', array('name' => $name));
    }
     
    public function createAction()
    {
         $producto = new Producto();
         $producto->setNombre('Lenovo g40');
         $producto->setPrecio('754800.99');
         $producto->setDescripcion('Negro,4Ram,500gb,w8');
         
         $em = $this->getDoctrine()->getManager();
         $em->persist($producto);
         $em->flush();
         
         return $this->render('DamdStoreBundle:Default:create.html.twig',
                             array("tipo"=>"Producto","id"=>$producto->getId()));
    }
     
    public function showAction($id)
    {
         $producto = $this->getDoctrine()
              ->getRepository('DamdStoreBundle:Producto')
              ->find($id);
         
         if(!$producto)
         {
             throw $this->createNotFoundException('No se encontro el producto con el id '.$id);
              
         }
         
         return $this->render('DamdStoreBundle:Default:show.html.twig',
                             array("producto"=>$producto));
    } 
     
     /**
      * @<opcionalborraresto>ParamConverter("post",class="DamdStoreBundle:Producto")
      */
    public function showParamConverterAction(Producto $producto){
       
         return $this->render('DamdStoreBundle:Default:show.html.twig',
                             array("producto"=>$producto));
    } 
     
     
}

<?php

namespace Damd\TwitterAPIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Damd\TwitterAPIBundle\Entity\Tweet;
use Damd\TwitterAPIBundle\Entity\Palabra;

class DefaultController extends Controller
{
     
  
    public function indexAction(Request $request)
    {
         
         $defaultData = array('tweet'=>'');
         
         $form = $this->createFormBuilder(array())
              ->add('tweet','text',array('label'=>"Palabra a Buscar : "))
              ->add('Buscar','submit')
              ->getForm();
         
         $form->handleRequest($request);
           $rawdata =  "";
          if($form->isValid())
          { 
             
             $json = Tweet::searchTweets($form->getData()['tweet']);
             /*$palabra = new Palabra();
             $palabra->setName($form->getData()['tweet']);
             $palabra->setFecha(new \DateTime());
              $em = $this->getDoctrine()->getManager();
              $em->persist($palabra);
              $em->flush();*/
             foreach($json->statuses as $user){
             $tweet = new Tweet();
             $tweet->setData($user);
             $rawdata[] = $tweet;
            }
          }
         
         $palabras = $this->getDoctrine()
                    ->getRepository('DamdTwitterAPIBundle:Palabra')
                    ->findAll();
         $arg = array('form'=>$form->createView(),
                     'twitts' => $rawdata,
                     'palabras'=>$palabras);
         
        return $this->render('DamdTwitterAPIBundle:Default:index.html.twig', $arg );
          
    }
     
     
     
}

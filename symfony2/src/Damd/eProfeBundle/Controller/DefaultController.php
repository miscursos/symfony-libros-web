<?php

namespace Damd\eProfeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('DamdeProfeBundle:Default:index.html.twig', array('name' => $name));
    }
}

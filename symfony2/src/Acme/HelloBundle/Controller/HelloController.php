<?php 
namespace Acme\HelloBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HelloController extends Controller {
     public function indexAction($name){
           return $this->render('AcmeHelloBundle:Hello:index.html.twig',array('name'=>$name));
     }
     
      public function cursoAction($name,$curso){
           $cursos = ["125"=>["titulo"=>"Symfony2",
                              "modulos"=>[["titulo"=>"Capitulo 1","img"=>"","descripcion"=>"Que es Symfony2"],
                                          ["titulo"=>"Capitulo 2","img"=>"","descripcion"=>"Crando Dunble"],
                                          ["titulo"=>"Capitulo 3","img"=>"","descripcion"=>"Mi primera Pagina"]
                                         ]
                             ]
                     ];
           
           if(isset($cursos[$curso])){
                $estado = "Solo haz compleado 10% del curos en 1año ¿que problemas tienes ?";
                $curso = $cursos[$curso];
           }
           else{
                if ($this->getCursoAction($curso)) {
                    $estado = "Hola $name no tiene permisos para entrar a este curso";
                    $curso = ["titulo"=>$curso,
                              "modulos"=>""
                              ];
                }
                else {
                    return $this->redirect($this->generateUrl("acme_hello_homepage",array('name'=>$name),301));
                }
           }
           return $this->render('AcmeHelloBundle:Hello:curso.html.twig',
                                  array('name'=>$name,'curso'=>$curso,'estado'=>$estado));
     }
     
     public function getCursoAction($titulo){
          
          $cursos= ["126"=>["titulo"=>"Symfony2Avanzado"],
                    "127"=>["titulo"=>"Symfony2BuenasPracticas"]
                   ];
          $response = null;
         
          foreach ($cursos as $curso){
               if ( $curso['titulo']==$titulo ) {
                    $response = $curso;
               }
                   
          }
          
          return $response ;
     }
     
     
}

?>
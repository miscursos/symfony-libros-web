<?php
// index.php
require_once 'vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$request = Request::createFromGLobals();


$uri = $request->getPathInfo();

if($uri == "/" ) {
     $response = list_action();
} elseif ( $uri == '/show' && $request->query->has('id')){
     $response = show_action($request->query->get('id'));
} else{
     $response = page_404_action();
}

$response->send();
?>
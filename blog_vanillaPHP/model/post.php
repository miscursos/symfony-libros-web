<?php
// model / post.php
 
function open_database_connection()
{
    $mysqli = new mysqli('localhost', 'root', 'root','blog');
     return $mysqli;
}
 
function close_database_connection($mysqli)
{
    $mysqli->close();
}
 
function get_all_posts()
{
    $mysqli = open_database_connection();
    $mysqli->real_query('SELECT * FROM post');
    $result = $mysqli->use_result();
   
    $posts = array();
    while ($row = $result->fetch_assoc()):
          $posts[] = $row;
    endwhile;
    
    close_database_connection($mysqli);
    return $posts;
}

function get_post($id)
{
     $mysqli = open_database_connection();
     $mysqli->real_query("SELECT * FROM post WHERE id=$id");
     $result = $mysqli->use_result();
     $post = null;
     while ($row = $result->fetch_assoc()):
          $post = $row;
     endwhile;
    close_database_connection($mysqli);
 
    return $post;
}
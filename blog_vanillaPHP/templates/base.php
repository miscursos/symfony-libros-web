<!DOCTYPE html>
<html>
     <head>
          <title><?= $title ?></title>
          <?= isset($style) ? $style : ""?>
     </head>
     <body>
          <?= $content ?>
          <?= isset($script) ? $script : ""?>
     </body>
</html>
<?php
use Symfony\Component\HttpFoundation\Response;


function render_template($path,array $args){
     extract($args);
     ob_start();
     require $path;
     $html = ob_get_clean();
     
     return $html;
}

function list_action(){
     $posts = get_all_posts();
     $html = render_template("templates/list.php",array('posts'=>$posts));
     return new Response($html);
}

function show_action($id){
     $post = get_post($id);
     
     $html = $post ? render_template("templates/read.php",array('post'=>$post)):page_404_action();
     return new Response($html);
}

function page_404_action(){
     $html = render_template("templates/404.php",array());
     return new Response($html,Response::HTTP_NOT_FOUND);
}

?>